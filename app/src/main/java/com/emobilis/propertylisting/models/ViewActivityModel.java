package com.emobilis.propertylisting.models;

import com.emobilis.propertylisting.views.ViewActivity;

public class ViewActivityModel {
    //variables
    private String id;
    private String name;
    private String phone;
    private String email;
    private String location;
    private String propertyPrice;
    private String propertyDesc;
    private String propertyName;
    private String propertyImage;
    private String showRoomDate;


    //empty constructor
    public ViewActivityModel(){

    }

    //constructor
    public ViewActivityModel(String id, String name, String phone, String email, String location, String propertyPrice,String propertyDesc, String propertyName
      , String propertyImage,String showRoomDate){

        this.id = id;
        this.name = name;
        this.phone = phone;
        this.email = email;
        this.location = location;
        this.propertyPrice = propertyPrice;
        this.propertyDesc = propertyDesc;
        this.propertyName = propertyName;
        this.propertyImage = propertyImage;
        this.showRoomDate = showRoomDate;

    }

    //get and set


    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    public String getPropertyPrice() {
        return propertyPrice;
    }

    public void setPropertyPrice(String propertyPrice) {
        this.propertyPrice = propertyPrice;
    }

    public String getPropertyDesc() {
        return propertyDesc;
    }

    public void setPropertyDesc(String propertyDesc) {
        this.propertyDesc = propertyDesc;
    }

    public String getPropertyName() {
        return propertyName;
    }

    public void setPropertyName(String propertyName) {
        this.propertyName = propertyName;
    }

    public String getPropertyImage() {
        return propertyImage;
    }

    public void setPropertyImage(String propertyImage) {
        this.propertyImage = propertyImage;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getShowRoomDate() {
        return showRoomDate;
    }

    public void setShowRoomDate(String showRoomDate) {
        this.showRoomDate = showRoomDate;
    }
}
