package com.emobilis.propertylisting;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;

public class SplashActivity extends AppCompatActivity {
    /** Duration of wait **/
    private final int SPLASH_DISPLAY_LENGTH = 3000;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        //handler
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                //new intent
                Intent intent = new Intent(SplashActivity.this,IntroSlider.class);
                startActivity(intent);

            }
        }, SPLASH_DISPLAY_LENGTH);
    }
}
