package com.emobilis.propertylisting.views;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.DialogFragment;

import android.app.DatePickerDialog;
import android.content.ContentResolver;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.View;
import android.webkit.MimeTypeMap;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.Spinner;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.emobilis.propertylisting.DatePickerDialogFragement;
import com.emobilis.propertylisting.R;
import com.emobilis.propertylisting.models.ViewActivityModel;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.android.material.textfield.TextInputEditText;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.OnProgressListener;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.StorageTask;
import com.google.firebase.storage.UploadTask;

import java.text.DateFormat;
import java.util.Calendar;

public class UploadActivity extends AppCompatActivity implements AdapterView.OnItemSelectedListener, DatePickerDialog.OnDateSetListener {
    //declare views
    TextInputEditText name,email,phone,prop_location,prop_desc,prop_price,date;
    Spinner prop_name;
    Button chooseImg,submit;
    //intialize request counter
    private static final int PICK_IMAGE_REQUEST = 1;
    //the path
    private Uri mImageUri;
    private ImageView mImageView;
    private ProgressBar mProgressBar;

    //firebase database and storage
    private StorageReference mStorageRef;
    private DatabaseReference mDatabaseRef;
    private StorageTask mUploadTask;

    //spinner item variable
    String houseCategory;
    //date from dialog picker
    String showroomDate;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_upload);

        chooseImg = findViewById(R.id.chooseImage);
        submit = findViewById(R.id.btnSubmit);
        name = findViewById(R.id.name);
        email = findViewById(R.id.email);
        phone = findViewById(R.id.phone);
        prop_desc = findViewById(R.id.desc_prop);
        prop_location = findViewById(R.id.location_prop);
        prop_price = findViewById(R.id.price_prop);
        prop_name = findViewById(R.id.name_prop); //spinner
        mImageView = findViewById(R.id.image);
        mProgressBar = findViewById(R.id.progress_bar);
        date = findViewById(R.id.date);


        date.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //launch a new instance of the DateDialogFragment
                DialogFragment datePicker = new DatePickerDialogFragement();
                datePicker.show(getSupportFragmentManager(), "date picker");
            }
        });

        //populating spinner with items
        ArrayAdapter<CharSequence> adapter = ArrayAdapter.createFromResource(this,
                R.array.categories, android.R.layout.simple_spinner_item);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        prop_name.setAdapter(adapter);
        prop_name.setOnItemSelectedListener(this);



        Button view = findViewById(R.id.view);
        view.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(UploadActivity.this,ViewActivity.class);
                startActivity(intent);
            }
        });

        //create the references to my db
        mStorageRef = FirebaseStorage.getInstance().getReference("uploadImages");
        mDatabaseRef = FirebaseDatabase.getInstance().getReference("uploadDetails");

        chooseImg.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                choose_Img();
            }
        });

        submit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mUploadTask != null && mUploadTask.isInProgress()) {
                    Toast.makeText(UploadActivity.this, "Upload still in progress", Toast.LENGTH_SHORT).show();
                } else {
                    uploadDetails();
                }
            }
        });
    }



    //intent to go to gallery
    private void choose_Img() {
        Intent intent = new Intent();
        intent.setType("image/*");
        intent.setAction(Intent.ACTION_GET_CONTENT);
        startActivityForResult(intent, PICK_IMAGE_REQUEST);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == PICK_IMAGE_REQUEST && resultCode == RESULT_OK
                && data != null && data.getData() != null) {
            mImageUri = data.getData();
            //setting users picked image to imageview display
            Glide
                    .with(this)
                    .load(mImageUri)
                    .into(mImageView);

        }
    }

    private String getFileExtension(Uri uri) {
        ContentResolver cR = getContentResolver();
        MimeTypeMap mime = MimeTypeMap.getSingleton();
        return mime.getExtensionFromMimeType(cR.getType(uri));
    }

    @Override
    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
          houseCategory = parent.getItemAtPosition(position).toString();
          Log.d("itemSelected","Category is " + houseCategory);
    }

    @Override
    public void onNothingSelected(AdapterView<?> parent) {

    }

    @Override
    public void onDateSet(DatePicker view, int year, int month, int dayOfMonth) {
        Calendar c = Calendar.getInstance();
        c.set(Calendar.YEAR, year);
        c.set(Calendar.MONTH, month);
        c.set(Calendar.DAY_OF_MONTH, dayOfMonth);
        showroomDate = DateFormat.getDateInstance(DateFormat.FULL).format(c.getTime());

        //edit text to display the date
        date.setText(showroomDate);
    }


    private void uploadDetails() {
        if (mImageUri != null) {
            StorageReference fileReference = mStorageRef.child(System.currentTimeMillis()
                    + "." + getFileExtension(mImageUri));
            //push image to storage
            mUploadTask = fileReference.putFile(mImageUri).addOnSuccessListener(new OnSuccessListener<UploadTask.TaskSnapshot>() {
                @Override
                public void onSuccess(UploadTask.TaskSnapshot taskSnapshot) {
                    //delaying the upload details until my image is pushed to bucket
                    Handler handler = new Handler();
                    handler.postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            mProgressBar.setProgress(0);
                        }
                    }, 500);
                    Toast.makeText(UploadActivity.this, "Upload successful", Toast.LENGTH_LONG).show();

                    //upload the text details to realtime db , download url
                    final String _name,_phone,_email,_nameProperty,_priceProperty,_descProperty,_locationProperty;

                    _name = name.getText().toString().trim();
                    _phone = phone.getText().toString().trim();
                    _email = email.getText().toString().trim();
                    _priceProperty = prop_price.getText().toString().trim();
                    _descProperty = prop_desc.getText().toString().trim();
                    _locationProperty = prop_location.getText().toString().trim();

                    //TASK CLASS : download url from my storage bucket
                    Task<Uri> urlTask = taskSnapshot.getStorage().getDownloadUrl(); //url to image from the storage
                    while(!urlTask.isSuccessful()); //checking whether i have my url
                    Uri downloadURL = urlTask.getResult(); //this is what we submit to the model
                    //create an id for unique identification of records
                    String uploadID = mDatabaseRef.push().getKey();
                    //create new instance of model class and push content according to model constructor
                    ViewActivityModel viewActivityModel = new ViewActivityModel(uploadID,_name,_email,_phone,_locationProperty,_priceProperty
                            ,_descProperty,houseCategory,downloadURL.toString(),showroomDate);
                    //so now at this point we push our details to our db
                    mDatabaseRef.child(uploadID).setValue(viewActivityModel);

                    //clear text
                    name.setText("");
                    phone.setText("");
                    email.setText("");
                    prop_price.setText("" );
                    prop_desc.setText("");
                    prop_location.setText("");



                }
            }).addOnFailureListener(new OnFailureListener() {
                        @Override
                        public void onFailure(@NonNull Exception e) {
                            Toast.makeText(UploadActivity.this, e.getMessage(), Toast.LENGTH_SHORT).show();
                        }
                    })
                    .addOnProgressListener(new OnProgressListener<UploadTask.TaskSnapshot>() {
                        @Override
                        public void onProgress(UploadTask.TaskSnapshot taskSnapshot) {
                            double progress = (100.0 * taskSnapshot.getBytesTransferred() / taskSnapshot.getTotalByteCount());
                            mProgressBar.setProgress((int) progress);
                        }
                    });


        } else {
            Toast.makeText(this, "No file selected", Toast.LENGTH_SHORT).show();
        }

    }



}
