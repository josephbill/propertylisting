package com.emobilis.propertylisting.views;

import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;

import android.Manifest;
import android.app.Activity;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.emobilis.propertylisting.R;

public class DetailsActivity extends AppCompatActivity {
    //variables
    String name,email,phoness,propertyName,propertyLocation,propertyPrice,image;
    TextView text_name, text_email, text_phone, text_prop_name, text_prop_desc, text_prop_price;
    ImageView imageView;
    private static final int REQUEST_CALL = 1;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_details);


        text_name = findViewById(R.id.name);
        text_email = findViewById(R.id.email);
        text_phone = findViewById(R.id.phone);
        text_prop_name = findViewById(R.id.propertyName);
        text_prop_desc = findViewById(R.id.propertyDesc);
        text_prop_price = findViewById(R.id.propertyPrice);
        imageView = findViewById(R.id.image);

        Intent intent = getIntent();
        name = intent.getStringExtra("name");
        email = intent.getStringExtra("email_property");
        phoness = intent.getStringExtra("phone_property");
        propertyName = intent.getStringExtra("name_property");
        propertyPrice = intent.getStringExtra("price_property");
        propertyLocation = intent.getStringExtra("location_property");
        image = intent.getStringExtra("image_property");

        //glide to view image
        Glide.with(this)
                .load(image)
                .fitCenter()
                .into(imageView);


        //fetch the image
        //u get multimedia files shared using the bundle getExtras method
//        Bundle bundle = getIntent().getExtras();
        //check if bundle has image
//        if (bundle != null){
//            image = bundle.getString("image_property");
//            //set image to the imageview
//        } else {
//            Toast.makeText(this, "No shared image ", Toast.LENGTH_SHORT).show();
//        }



        //set text
        text_name.setText("Uploaded by: " + name);
        text_prop_price.setText("Price: " + propertyPrice);
        text_prop_desc.setText("Location " + propertyLocation);
        text_prop_name.setText("Name of Property: " + propertyName);
        text_phone.setText("Phone Contact: " + phoness);
        text_email.setText("Email Contact: " + email);

    }

    public void email(View v){
         Intent intent = new Intent(Intent.ACTION_SEND);
         intent.putExtra(Intent.EXTRA_TEXT,"I want to inquire about " + propertyName);
         intent.putExtra(Intent.EXTRA_SUBJECT,"Inquiry");
         intent.putExtra(Intent.EXTRA_EMAIL,email);
         intent.setType("text/message");
         startActivity(Intent.createChooser(intent,"Choose Email App"));


    }

    public void phone(View v){
        if (ContextCompat.checkSelfPermission(this,
                Manifest.permission.CALL_PHONE) != PackageManager.PERMISSION_GRANTED){
            //i need to alert the user to allow the app to make a call
            ActivityCompat.requestPermissions((Activity) this,
                    new String[]{Manifest.permission.CALL_PHONE}, REQUEST_CALL);
        } else {
            //if the user has allowed app to make a call , launch intent here
            String phone = "tel:" + phoness;
            startActivity(new Intent(Intent.ACTION_DIAL, Uri.parse(phone)));
        }
    }
}
