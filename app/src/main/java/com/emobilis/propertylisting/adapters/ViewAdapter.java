package com.emobilis.propertylisting.adapters;

import android.content.Context;
import android.content.Intent;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.emobilis.propertylisting.R;
import com.emobilis.propertylisting.models.ViewActivityModel;
import com.emobilis.propertylisting.views.DetailsActivity;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

import java.util.ArrayList;
import java.util.List;

public class ViewAdapter extends RecyclerView.Adapter<ViewAdapter.ViewHolderAdapter> {
    Context context;
    List<ViewActivityModel> viewActivityModels;


    public ViewAdapter(Context context,List<ViewActivityModel> viewActivityModels1){
        this.context = context;
        this.viewActivityModels = viewActivityModels1;
    }



    public class ViewHolderAdapter extends RecyclerView.ViewHolder{
        TextView tvProperyName,tvPropertyPrice,tvPropertyLocation;
        Button btnSee;
        ImageView imageView;
        CardView cardView;
        public ViewHolderAdapter(@NonNull View itemView) {
            super(itemView);

            tvPropertyLocation = itemView.findViewById(R.id.locationProperty);
            tvProperyName = itemView.findViewById(R.id.nameProperty);
            tvPropertyPrice = itemView.findViewById(R.id.priceProperty);
            btnSee = itemView.findViewById(R.id.seeMore);
            imageView = itemView.findViewById(R.id.imageProperty);
            cardView = itemView.findViewById(R.id.cardClick);

        }
    }


    @NonNull
    @Override
    public ViewAdapter.ViewHolderAdapter onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.property_item, parent, false);
        ViewAdapter.ViewHolderAdapter viewHolderAdapter = new ViewAdapter.ViewHolderAdapter(v); //new instance
        return viewHolderAdapter;
    }

    @Override
    public void onBindViewHolder(@NonNull ViewAdapter.ViewHolderAdapter holder, final int position) {
        ViewActivityModel viewActivityModel = viewActivityModels.get(position);

        holder.tvProperyName.setText("Name of Property: " + viewActivityModel.getPropertyName());
        holder.tvPropertyPrice.setText("Price: " + viewActivityModel.getPropertyPrice());
        holder.tvPropertyLocation.setText("Located At: " + viewActivityModel.getLocation());
        Glide.with(context)
                .load(viewActivityModel.getPropertyImage())
                .fitCenter()
                .into(holder.imageView);

        holder.btnSee.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(context, DetailsActivity.class);
                intent.putExtra("name_property",viewActivityModels.get(position).getPropertyName());
                intent.putExtra("price_property",viewActivityModels.get(position).getPropertyPrice());
                intent.putExtra("location_property",viewActivityModels.get(position).getLocation());
                intent.putExtra("phone_property",viewActivityModels.get(position).getPhone());
                intent.putExtra("email_property",viewActivityModels.get(position).getEmail());
                intent.putExtra("name",viewActivityModels.get(position).getName());
                intent.putExtra("image_property",viewActivityModels.get(position).getPropertyImage());
                context.startActivity(intent);
            }
        });

        //string id , email and phone
        String id = viewActivityModels.get(position).getId();
        String email = viewActivityModels.get(position).getEmail();
        String phone = viewActivityModels.get(position).getPhone();
        String location = viewActivityModels.get(position).getLocation();
        String price =  viewActivityModels.get(position).getPropertyPrice();
        String desc =  viewActivityModels.get(position).getPropertyDesc();
        String name_prop =  viewActivityModels.get(position).getPropertyName();
        String showroomDate =  viewActivityModels.get(position).getPropertyPrice();
        String propertyImage =  viewActivityModels.get(position).getPropertyImage();
        String name =  viewActivityModels.get(position).getName();
        //set up onClick
        holder.cardView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //update and delete dialog
                showUpdateDeleteDialog(id,email,phone,location,price,desc,name_prop,showroomDate,propertyImage,name);
            }
        });

    }

    private void showUpdateDeleteDialog(String id, String email, String phone, String location, String price, String desc, String name_prop, String showroomDate, String propertyImage, String name) {

        //raising a dialog
        AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(context);
        LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        final View dialogView = inflater.inflate(R.layout.update_property, null);
        dialogBuilder.setView(dialogView);

        //referencing my views
        final EditText editPrice = dialogView.findViewById(R.id.editPrice);
        final EditText editEmail = dialogView.findViewById(R.id.editEmail);
        final EditText editPhone = dialogView.findViewById(R.id.editPhone);
        final Button btnUpdate = dialogView.findViewById(R.id.buttonUpdateProperty);
        final Button btnDelete = dialogView.findViewById(R.id.buttonDeleteProperty);

        //set a title to builder
        dialogBuilder.setTitle("Update or Delete this property");
        //create and show the dialog
        final AlertDialog b = dialogBuilder.create();
        b.show();

        btnUpdate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //get user input
                String new_phone = editPhone.getText().toString().trim();
                String new_email = editEmail.getText().toString().trim();
                String new_price = editPrice.getText().toString().trim();

                //validate one of the options
                if (TextUtils.isEmpty(new_price)){
                    Toast.makeText(context, "You cannot update empty record", Toast.LENGTH_SHORT).show();
                } else {
                    updateProperty(id,new_email,new_phone,new_price,location,desc,name_prop,showroomDate,propertyImage,name);
                    b.dismiss();
                }
            }
        });

        btnDelete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                deleteProperty(id);
                b.dismiss();
            }
        });

    }

    private boolean updateProperty(String id, String new_email, String new_phone, String new_price, String location, String desc, String name_prop, String showroomDate, String propertyImage, String name) {
        //ref to the firebase DB
        DatabaseReference databaseReference = FirebaseDatabase.getInstance().getReference("uploadDetails").child(id);
        //push new values to model based on id
        //create a new Object of model class
        ViewActivityModel viewActivityModel = new ViewActivityModel(id,name,new_phone,new_email,location,new_price,desc,name_prop,propertyImage,showroomDate);
        databaseReference.setValue(viewActivityModel);



        return true;

    }


    private boolean deleteProperty(String id){
        //reference the firebase database node
        DatabaseReference databaseReference = FirebaseDatabase.getInstance().getReference("uploadDetails").child(id);
        //use removeValue to delete
        databaseReference.removeValue();

        return true;

    }

    @Override
    public int getItemCount() {
        return viewActivityModels.size();
    }
}
