package com.emobilis.propertylisting;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;

import com.emobilis.propertylisting.authentication.LoginActivity;
import com.emobilis.propertylisting.views.ViewActivity;

public class SelectionChoice extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_selection_choice);
    }

    public void upload(View v){
        Intent intent = new Intent(SelectionChoice.this, LoginActivity.class);
        startActivity(intent);
    }

    public void view(View v){
        Intent intent = new Intent(SelectionChoice.this, ViewActivity.class);
        startActivity(intent);
    }
}
